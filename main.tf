# EKS Terraform module

module "eks" {
  source                = "./modules/eks"
  cluster-name          = "${var.cluster-name}"
  aws-region            = "${var.aws-region}"
  node-instance-type    = "${var.node-instance-type}"
  desired-capacity      = "${var.desired-capacity}"
  max-size              = "${var.max-size}"
  min-size              = "${var.min-size}"
  ssh-key-name          = "${var.ssh-key-name}"
  vpc-subnet-cidr       = "${var.vpc-subnet-cidr}"
  worker-public-ip      = "${var.worker-public-ip}"
  worker-bootstrap-args = "${var.worker-bootstrap-args}"
}
